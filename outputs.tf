output "lb_endpoint" {
  value = "https://${aws_lb.tf-terraform.dns_name}"
}

output "application_endpoint" {
  value = "https://${aws_lb.tf-terraform.dns_name}/index.php"
}

output "asg_name" {
  value = aws_autoscaling_group.tf-terraform.name
}