terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.77.0"

  name = "trey-terraform-main-vpc"
  cidr = "10.0.0.0/16"

  azs                  = data.aws_availability_zones.available.names
  public_subnets       = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  enable_dns_hostnames = true
  enable_dns_support   = true
}

data "aws_ami" "amazon-linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-hvm-*-x86_64-ebs"]
  }
}

resource "aws_launch_configuration" "tf-terraform" {
  name_prefix     = "trey-terraform-asg-"
  image_id        = data.aws_ami.amazon-linux.id
  instance_type   = "t2.micro"
  user_data       = file("user-data.sh")
  security_groups = [aws_security_group.tf-terraform_instance.id]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "tf-terraform" {
  name                 = "tf-terraform"
  min_size             = 1
  max_size             = 3
  desired_capacity     = 1
  launch_configuration = aws_launch_configuration.tf-terraform.name
  vpc_zone_identifier  = module.vpc.public_subnets

  tag {
    key                 = "Name"
    value               = "Trey Terraform ASG - tf-terraform"
    propagate_at_launch = true
  }
}

resource "aws_lb" "tf-terraform" {
  name               = "trey-terraform-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.tf-terraform_lb.id]
  subnets            = module.vpc.public_subnets
}

resource "aws_lb_listener" "tf-terraform" {
  load_balancer_arn = aws_lb.tf-terraform.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tf-terraform.arn
  }
}

resource "aws_lb_target_group" "tf-terraform" {
  name     = "trey-terrform"
  port     = 80
  protocol = "HTTP"
  vpc_id   = module.vpc.vpc_id
}


resource "aws_autoscaling_attachment" "tf-terraform" {
  autoscaling_group_name = aws_autoscaling_group.tf-terraform.id
  alb_target_group_arn   = aws_lb_target_group.tf-terraform.arn
}

resource "aws_security_group" "tf-terraform_instance" {
  name = "trey-terrform-tf-terraform-instance"
  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.tf-terraform_lb.id]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.tf-terraform_lb.id]
  }

  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group" "tf-terraform_lb" {
  name = "trey-terrform-tf-terraform-lb"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = module.vpc.vpc_id
}

# resource "aws_ecs_cluster" "main" {
#   name = "trey-terraform-cluster"
# }

# resource "aws_ecs_service" "main" {
#  name                               = "trey-terraform-service"
#  cluster                            = aws_ecs_cluster.main.id
#  task_definition                    = aws_ecs_task_definition.main.arn
#  desired_count                      = 2
#  deployment_minimum_healthy_percent = 50
#  deployment_maximum_percent         = 200
#  launch_type                        = "FARGATE"
#  scheduling_strategy                = "REPLICA"
 
#  network_configuration {
#    security_groups  = ["sg-841f3efb"]
#    subnets          = ["subnet-f077a5b8"]
#    assign_public_ip = true
#  }
# }

# resource "aws_ecs_task_definition" "main" {
#   family = "trey-terraform"
#   network_mode             = "awsvpc"
#   requires_compatibilities = ["FARGATE"]
#   cpu                      = 256
#   memory                   = 512
#   execution_role_arn       = "arn:aws:iam::752436870060:role/ecsTaskExecutionRole_trey"
#   container_definitions = jsonencode([{
#    name        = "trey-terraform-container"
#    image       = "trey_node_api:latest"
#    essential   = true,
#    portMappings = [{
#      protocol      = "tcp"
#      containerPort = 8081
#      hostPort      = 8081
#     }]
#   }])
# }
